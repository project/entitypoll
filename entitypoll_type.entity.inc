<?php

/**
 * The class used for entityform type entities
 */
class EntitypollType extends Entity {

  public $name;
  public $label;
  public $data;
  public $disabled;

  public function __construct($values = array()) {
    parent::__construct($values, 'entitypoll_type');
  }
}

/**
 * The Controller for Entityform entities
 */
class EntitypollTypeController extends EntityAPIControllerExportable {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  /**
   * Create a entityform type - we first set up the values that are specific
   * to our entityform type schema but then also go through the EntityAPIController
   * function.
   *
   * @param $type
   *   The machine-readable type of the entityform.
   *
   * @return
   *   A entityform type object with all default fields initialized.
   */
  public function create(array $values = array(), $load_defaults = FALSE) {
    // Add values that are specific to our Entityform

    /*$default_values = variable_get('entitypoll_type_defaults', array());
    if (empty($default_values)) {
      $default_values = array();
    }
    $values += $default_values;
    $values += array(
      'id' => '',
      'is_new' => TRUE,
    );
    if (!isset($values['data'])) {
      $values['data'] = array();
    }
    if ($load_defaults) {
      $values['data'] += array(
        'submissions_view' => 'entityforms',
        'user_submissions_view' => 'user_entityforms',
        'preview_page' => 0,
      );
      $values['data'] += $this->get_default_text_values();
    }
    else {
      if ($values['is_new']) {
        $values['data']['submissions_view'] = 'default';
        $values['data']['user_submissions_view'] = 'default';
      }
    }*/
    $entityform_type = parent::create($values);
    return $entityform_type;
  }

  /*
   * Overridden to Load file
   */
  public function view($entities, $view_mode = 'full', $langcode = NULL, $page = NULL) {
    $view = parent::view($entities, $view_mode, $langcode, $page);
    //foreach ($entities as $entity_id => $entity) {
    //  module_load_include('inc', 'entityform', 'entityform.admin');
    //  $view[$this->entityType][$entity->type]['form'] = entityform_form_wrapper(entityform_empty_load($entity->type), 'submit', 'embedded');
    //}
    return $view;
  }



  /**
   * Overridden to clear cache.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    $return = parent::save($entity, $transaction);

    // Reset the entityform type cache. We need to do this first so
    // menu changes pick up our new type.
    //entityform_type_cache_reset();
    // Clear field info caches such that any changes to extra fields get
    // reflected.
    //field_info_cache_clear();



    //field_create_instance($this->questionField($entity->name));
    //field_info_cache_clear();

    return $return;
  }

  /**
   * Overridden to delete aliases and clear cache.
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    $entities = $ids ? $this->load($ids) : FALSE;
    if ($entities) {
      parent::delete($ids, $transaction);

      // Clear field info caches such that any changes to extra fields get
      // reflected.
      //field_info_cache_clear();
      // Reset the entityform type cache.
      //entityform_type_cache_reset();
      //foreach($entities as $entity) {
      //  field_delete_instance($this->questionField($entity->name));
      //}
    }
  }

  /*
   * Overridden to exclude pid in alias Export
   */
  /*public function export($entity, $prefix = '') {
    if (module_exists('path')) {
      foreach ($entity->paths as &$path) {
        unset($path['pid']);
      }
    }
    return parent::export($entity, $prefix);
  }*/
  /**
   * {@inheritdoc}
   */
  function load($ids = array(), $conditions = array()) {
    $entities = parent::load($ids, $conditions);
    return $entities;
  }
}