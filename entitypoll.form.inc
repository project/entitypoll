<?php


/**
 * exchanger_content entity edit/create form.
 */
function entitypoll_form($form, &$form_state) {
  $entity = isset($form['#entity']) ? $form['#entity'] : $form_state['entitypoll'];

  $form['question'] = array(
    '#title' => t('Question'),
    '#type' => 'textfield',
    '#default_value' => isset($entity->question) ? $entity->question : '',
    '#description' => t('Question of the poll.'),
    '#required' => TRUE,
    '#weight' => -50,
  );

  field_attach_form('entitypoll', $entity, $form, $form_state);

  $form['entitypoll_choices'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Container of choices'),
    '#attached' => array(
      'css' => array(
        // We are not using fields here, but we are reusing some CSS to style
        // our choices like fields.
        drupal_get_path('module', 'field') . '/theme/field.css',
      ),
    ),
    '#attributes' => array(
      'class' => array('field-multiple-table'),
    ),
  );

  $wrapper_id = drupal_html_id("entitypoll-choices-wrapper");
  $form['entitypoll_choices']['wrapper'] = array(
    '#type' => 'container',
    '#prefix' => "<div id=\"$wrapper_id\">",
    '#suffix' => '</div>',
  );

  if (empty($form['#parents'])) {
    $choices_state = &$form_state['entitypoll_choices']['entitypoll_choices'];
  } else {
    $choices_state = &$form_state['entitypoll_choices'][$form['#parents'][0]];
  }

  $entitypoll_id = $entity->entitypoll_id;

   // Load all existing choices of this entitypoll.
   if ($entitypoll_id != 0) {
     $query = new EntityFieldQuery();
     $query->entityCondition('entity_type', 'entitypoll_choice', '=');
     $query->propertyCondition('entitypoll', $entitypoll_id, '=');
     $query->propertyOrderBy('weight');
     $result = $query->execute();

     if (isset($result['entitypoll_choice'])) {
       $choices_loaded = entity_load('entitypoll_choice', array_keys($result['entitypoll_choice']));
       foreach ($choices_loaded as $choice_loaded) {
         // If choice is already added to $form_state, we are skipping it.
         if (!empty($choices_state)) {
           foreach ($choices_state as $choice_state) {
             if ($choice_state->pcid == $choice_loaded->pcid) {
               continue 2;
             }
           }
         }
         $choices_state[] = $choice_loaded;
       }
     }
   }


   if (!empty($choices_state)) {

     $form['entitypoll_choices']['wrapper']['entitypoll_choices'] = array(
       '#theme' => 'entitypoll_choices',
       '#type' => 'container',
     );

     $choices_element_wrapper = &$form['entitypoll_choices']['wrapper']['entitypoll_choices'];

     foreach ($choices_state as $key => $choice_state) {

         // Hide choices that has been removed from form
       if (isset($choice_state->is_removed)) {
         continue;
       }

       $choices_element_wrapper[$key] = array(
         '#type' => 'container',
         '#tree' => TRUE,
         'weight' => array(
           '#type' => 'textfield',
           '#size' => 5,
           '#default_value' => $choice_state->weight,
         ),
         'type' => array(
           '#type' => 'hidden',
           '#value' => $choice_state->type,
         ),
         'pcid' => array(
           '#type' => 'hidden',
           '#value' => isset($choice_state->pcid) ? $choice_state->pcid : 0,
         ),
         'remove' => array(
           '#type' => 'submit',
           '#value' => t('Remove'),
           '#submit' => array('entitypoll_remove_submit'),
           '#name' => "entitypoll_choices-$key-remove",
           '#limit_validation_errors' => array(),
           '#ajax' => array(
             'callback' => 'entitypoll_form_ajax_remove_callback',
             'wrapper' => $wrapper_id,
             'effect' => 'fade',
           ),
         ),
       );

       $choices_element_wrapper[$key]['#parents'] = array_merge($form['#parents'], array('entitypoll_choices', 'wrapper', 'entitypoll_choices', $key));
       entitypoll_choice_form_attach_elements($choice_state, $choices_element_wrapper[$key], $form_state);
     }
   }


   $form['entitypoll_choices']['actions'] = array(
     '#type' => 'container',
   );

  $types = entitypoll_choice_get_types();
  if (empty($types)) {

    $form['entitypoll_choices']['wrapper']['no_bundles'] = array(
      '#type' => 'markup',
      '#markup' => '<em>' . t('There are no bundles of entitypoll choices to add.') . '</em>',
    );
  }
  else {
    $select_list = array();
    foreach ($types as $type) {
      $select_list[$type->name] = t($type->label);
    }


    $form['entitypoll_choices']['wrapper']['add_more'] = array(
      '#type' => 'container',
    );

    $form['entitypoll_choices']['wrapper']['add_more']['entitypoll_choice_type'] = array(
      '#type' => 'select',
      '#title' => t('Choice type'),
      '#options' => $select_list,
      '#default_value' => key($select_list),
    );


    $form['entitypoll_choices']['wrapper']['add_more']['add_more'] = array(
      '#type' => 'submit',
      '#value' => t('Add choice'),
      '#submit' => array('entitypoll_add_more_submit'),
      '#limit_validation_errors' => array(
        array_merge($form['#parents'], array('entitypoll_choices', 'wrapper', 'add_more', 'entitypoll_choice_type')),
      ),
      '#ajax' => array(
        'callback' => 'entitypoll_form_ajax_add_more_callback',
        'wrapper' => $wrapper_id,
        'effect' => 'fade',
      ),
    );

    if (isset($form['entitypoll_choices']['wrapper']['#parents'])) {
      $form['entitypoll_choices']['wrapper']['add_more']['entitypoll_choice_type']['#parents'] =  array_merge($form['entitypoll_choices']['wrapper']['#parents'], array('add_more', 'entitypoll_choice_type'));
      $form['entitypoll_choices']['wrapper']['add_more']['add_more']['#parents'] = array_merge($form['entitypoll_choices']['wrapper']['#parents'], array('add_more', 'add_more'));
    }
  }

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save entitypoll'),
    ),
    'delete' => array(
      '#type' => 'submit',
      '#value' => t('Delete entitypoll'),
      '#access' => entitypoll_access('delete', $entity),
      '#submit' => array('entitypoll_form_submit_delete'),
    ),
  );

  return $form;
}


/**
 * Implements hook_form_validate().
 *
 * @param $form
 * @param $form_state
 */
function entitypoll_form_validate($form, &$form_state) {
  $form_state['values']['type'] = $form_state['entitypoll']->type;
  entity_form_field_validate('entitypoll', $form, $form_state);
}


/**
 * Submit callback for entitypoll form.
 */
function entitypoll_form_submit($form, &$form_state) {
  entitypoll_form_submit_prepare($form, $form_state);
  $entity = isset($form['#entity']) ? $form['#entity'] : $form_state['entitypoll'];
  entity_save('entitypoll', $entity);
  drupal_set_message(t('Entitypoll %question has been saved.', array('%question' => $entity->question)));
}


/**
 * Submit callback for entitypoll form.
 */
function entitypoll_form_submit_prepare($form, &$form_state) {

  $parent = $form_state['triggering_element']['#array_parents'][0];
  $entitypoll = isset($form['#entity']) ? $form['#entity'] : $form_state['entitypoll'];

  entity_form_submit_build_entity('entitypoll', $entitypoll, $form, $form_state);

  // Choices
  $entitypoll_choices = &$form['entitypoll_choices']['wrapper']['entitypoll_choices'];

  //$entitypoll->choices = array();
  $form_state['entitypoll_choices'][$parent] = array();

  //global $language; // TODO ?
  $weight = 0;
  //$entitypoll_choices['#array_parents']
  $choices_values = drupal_array_get_nested_value($form_state['values'], $entitypoll_choices['#array_parents']);

  foreach ($choices_values as $key2 => $choice) {
    $values = drupal_array_get_nested_value($form_state['values'], $entitypoll_choices[$key2]['#parents']);

    if ($values['pcid'] == 0) {
      $values['entitypoll'] = $entitypoll->entitypoll_id;
      //$values['language'] = $language->language; // TODO ?
      $choice = entity_create('entitypoll_choice', $values);
    }
    else {
      $choice = entity_load_single('entitypoll_choice', $values['pcid']);
      foreach ($values as $property => $value) {
        $choice->$property = $value;
      }
    }
    $choice->weight = $weight;
    $form_state['entitypoll_choices'][$parent][$weight] = $choice;
    $weight++;
  }

  $entitypoll->choices = $form_state['entitypoll_choices'][$parent];
}

/**
 * Submit callback responsible for deleting an entity.
 */
function entitypoll_form_submit_delete($form, &$form_state) {
  $form_state['redirect'] = 'entitypoll/' . $form_state['entitypoll']->entitypoll_id . '/delete';
}


/**
 * Submit callback.
 */
function entitypoll_add_more_submit($form, &$form_state) {
  global $user;

  $parents = _entitypoll_form_triggering_element_root_parents($form_state['triggering_element']['#parents']);
  $parent = $form_state['triggering_element']['#array_parents'][0];

  if (!empty($form_state['entitypoll_choices'][$parent])) {
    // We need to find highest weight, and increase it by one, to be sure that
    // added element has highest weight.
    $weights = array();
    foreach($form_state['entitypoll_choices'][$parent] as $choice) {
        $weights[] = $choice->weight;
    }
    $weight = max($weights) + 1;
  }

  $choice_type_parents = array_merge($parents, array('wrapper', 'add_more', 'entitypoll_choice_type'));
  $choice_type = drupal_array_get_nested_value($form_state['values'], $choice_type_parents);


  $entity = entity_create('entitypoll_choice', array(
    'type' => $choice_type,
    'weight' => isset($weight) ? $weight : 0,
    'voting_type' => 'default',
    'uid' => $user->uid
    )
  );

  $form_state['entitypoll_choices'][$parent][] = $entity;

  $form_state['rebuild'] = TRUE;
}


/**
 * Submit callback.
 */
function entitypoll_remove_submit($form, &$form_state) {

  $parents = _entitypoll_form_triggering_element_root_parents(array_merge($form_state['triggering_element']['#parents'], array('entitypoll_choices')));
  $values = drupal_array_get_nested_value($form_state['values'], $parents);

  $parent = $form_state['triggering_element']['#array_parents'][0];

  foreach ($values as $key => $button) {
    if (isset($form_state['entitypoll_choices'][$parent][$key])) {
      if ($form_state['entitypoll_choices'][$parent][$key]->pcid != 0) {
        $form_state['entitypoll_choices'][$parent][$key]->is_removed = TRUE;
      }
      else {
        unset($form_state['entitypoll_choices'][$parent][$key]);
      }
    }
  }

  $form_state['rebuild'] = TRUE;
}

/**
 * Ajax callback.
 */
function entitypoll_form_ajax_add_more_callback($form, $form_state) {
  $parents = _entitypoll_form_triggering_element_root_parents($form_state['triggering_element']['#array_parents']);
  $choices_element = _entitypoll_array_get_nested_element($form, $parents);

  $choices_array = element_children($choices_element['entitypoll_choices']['wrapper']['entitypoll_choices']);

  foreach ($choices_array as $key) {
    $choices_element['entitypoll_choices']['wrapper']['entitypoll'][$key]['#parents'] = array_merge($choices_element['entitypoll_choices']['wrapper']['entitypoll_choices']['#parents'], array($key));
  }

  $wrapper_element = $choices_element['wrapper'];
  return $wrapper_element;
}

/**
 * Ajax callback.
 */
function entitypoll_form_ajax_remove_callback($form, $form_state) {
  $parents = _entitypoll_form_triggering_element_root_parents($form_state['triggering_element']['#array_parents']);
  $wrapper_element = _entitypoll_array_get_nested_element($form, $parents);
  return $wrapper_element;
}



/**
 * Helper function for getting nested element from renderable array.
 */
function _entitypoll_array_get_nested_element($form, $parents) {
  $element = $form;
  foreach ($parents as $parent) {
    $element = &$element[$parent];
  }
  return $element;
}


/**
 * Helper function.
 *
 * We are going up from the triggering element just to be sure that we are
 * dealing with the right form, because when using Inline Entity Form, we
 * can have multiple entitypoll forms in one structure.
 */
function _entitypoll_form_triggering_element_root_parents($parents, $levels_up = 3) {
  for ($i = 0; $i < $levels_up; $i++) {
    array_pop($parents);
  }
  return $parents;
}


/**
 * This is entitypoll form visible to end users, allows for voting.
 */
function entitypoll_widget_display_form($form, &$form_state, $entitypoll, $entitypoll_type, $view_mode, $choices_view_mode, $widget_machine_name, $widget) {

  $widget_config = !empty($entitypoll_type->data['widget'][$view_mode]['config']) ? $entitypoll_type->data['widget'][$view_mode]['config'] : array();

  $form = array();
  $form['entitypoll_choices'] = array(
    '#type' => 'container',
  );

  $form['entitypoll_choices'] += $widget['widget display callback']($entitypoll, $view_mode, $choices_view_mode, $widget_config);

  $form['widget'] = array(
    '#type' => 'value',
    '#value' => $widget_machine_name,
  );
  $form['entitypoll_id'] = array(
    '#type' => 'value',
    '#value' => $entitypoll->entitypoll_id,
  );

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Vote'),
    ),
  );
  return $form;
}



/**
 * Submit callback of entitypoll form displayed to end users.
 */
function entitypoll_widget_display_form_submit($form, &$form_state) {

  $widgets_info = entitypoll_widget_info();
  $voting_types_info = entitypoll_voting_type_info();
  $widget_machine_name = $form_state['values']['widget'];
  $widget = $widgets_info[$widget_machine_name];

  $votes_array = $widget['votes callback']($form_state['values']['entitypoll_choices']);

  $votes = array();
  foreach ($votes_array as $choice_id => $votes_count) {
    $votes[] = array(
      'entity_type' => 'entitypoll_choice',
      'entity_id' => $choice_id,
      'value_type' => $voting_types_info[$widget['voting type']]['votes type'],
      'value' => $votes_count,
      'vote_source' => ip_address(),
    );
  }

  $entitypoll = entitypoll_load($form_state['values']['entitypoll_id']);
  drupal_alter('entitypoll_votes', $entitypoll, $votes);
  if (!empty($votes)) {
    votingapi_set_votes($votes);
  }
  $entitypoll = entitypoll_load($form_state['values']['entitypoll_id']);
  module_invoke_all('entitypoll_post_vote', $entitypoll, $votes, $widget_machine_name);
}


/**
 * Default callback building a form of choices.
 */
function entitypoll_widget_display_form_default_callback($entitypoll, $view_mode, $choices_view_mode, $widget_config) {
  $element = array();

  foreach ($entitypoll->choices as $choice) {
    $choice->entitypoll_view_mode = $view_mode;
    $element[] = entity_view('entitypoll_choice', array($choice), $choices_view_mode);
  }
  return $element;
}