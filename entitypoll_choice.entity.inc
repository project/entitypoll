<?php


/**
 * Class Entitypoll
 */
class EntitypollChoice extends Entity {
  /**
   * The internal ID of this content.
   *
   * @var int
   */
  public $pcid = 0;

  public $type;

  public $entitypoll;

  public $weight;

  public $created;

  public $updated;

  /**
   * In this mode no default properties are set, you need to provide them
   * yourself.
   *
   * @var bool
   */
  public $migrate_mode = FALSE;

  /**
   * Creates a new entity.
   *
   * @see entity_create()
   */
  public function __construct(array $values = array()) {
    // If this is new then set the type first.
    if (isset($values['is_new'])) {
      $this->type = isset($values['type']) ? $values['type'] : '';
    }

    parent::__construct($values, 'entitypoll_choice');
  }

  /**
   * Label callback.
   */
  protected function defaultLabel() {
    $this_wrapped = entity_metadata_wrapper('entitypoll_choice', $this);
    return $this_wrapped->entitypoll->label();
  }

  /**
   * Get votes casted on our choice.
   *
   * TODO: it should be an entity property.
   */
  public function getVotes($function) {
    $entitypoll = entitypoll_load($this->entitypoll);
    $entitypoll_type = entitypoll_type_load($entitypoll->type);

    $voting_type_info = entitypoll_voting_type_info();
    $voting_type = $entitypoll_type->voting_type;

    $criteria = array(
      'entity_type' => $this->entityType(),
      'entity_id' => $this->pcid ? $this->pcid : 0,
      'value_type' => $voting_type_info[$voting_type]['votes type'],
      'function' => $function,
    );
    $votingapi_results = votingapi_select_results($criteria);
    $result = empty($votingapi_results) ? 0 : $votingapi_results[0]['value'];
    return $result;
  }

}

/**
 * Class EntitypollController
 */
class EntitypollChoiceController extends EntityAPIController {

  /**
   * {@inheritdoc}
   */
  public function create(array $values = array()) {
    $values += array(
      'created' => REQUEST_TIME,
    );
    return parent::create($values);
  }

  /**
   * {@inheritdoc}
   */
  function load($ids = array(), $conditions = array()) {
    $entities = parent::load($ids, $conditions);
    foreach ($entities as $entity) {
      // Cast non-string scalars to their original types, because some backends
      // store/return all variables as strings.
      $entity->pcid = (int) $entity->pcid;
    }
    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  function save($entity, DatabaseTransaction $transaction = NULL) {
    if ($entity->migrate_mode === FALSE) {
      if (isset($entity->is_new)) {
        $entity->created = REQUEST_TIME;
      }
      $entity->updated = REQUEST_TIME;
    }
    return parent::save($entity, $transaction);
  }

  /**
   * {@inheritdoc}
   */
  public function buildContent($entitypoll_choice, $view_mode = 'full', $langcode = NULL, $content = array()) {

    // If we don't know entitypoll view mode - we cannot do much. We need this
    // knowledge to get entitypoll type settings, which are configurable by
    // view mode.
    if (empty($entitypoll_choice->entitypoll_view_mode)) {
      $content['entitypoll_choice_widget']['#markup'] = $content['entitypoll_choice_result']['#markup'] = t('\'widget display callback\' error: Choice doesn\'t know Entitypoll view mode.');
      return parent::buildContent($entitypoll_choice, $view_mode, $langcode, $content);
    }

    $entitypoll_view_mode = $entitypoll_choice->entitypoll_view_mode;

    $entitypoll = entitypoll_load($entitypoll_choice->entitypoll);
    $entitypoll_type = entitypoll_get_type($entitypoll->type);

    $votes = array();

    // Widget
    $widget_output = array();
    if (entitypoll_widget_access('vote', $entitypoll)) {
      $widget_machine_name = !empty($entitypoll_type->data['widget'][$entitypoll_view_mode]['widget']) ? $entitypoll_type->data['widget'][ $entitypoll_view_mode]['widget'] : FALSE;
      $widgets_info = entitypoll_widget_info();
      $widget = !empty($widgets_info[$widget_machine_name]) ? $widgets_info[$widget_machine_name] : FALSE;

      if (!$widget) {
        $widget_output['#markup'] = t('No widget.');
      }
      else {
        $widget_config = !empty($entitypoll_type->data['widget'][$view_mode]['config']) ? $entitypoll_type->data['widget'][$view_mode]['config'] : array();

        // Load a file with callbacks.
        if (isset($widget['file'])) {
          entitypoll_include_file($widget['module'], $widget['file']);
        }
        $widget_output = $widget['element callback']($entitypoll, $entitypoll_choice, $view_mode, $widget_config, $votes);
      }
    }
    $content['entitypoll_choice_widget'] = $widget_output;


    // Result
    $result_output = array();
    if (entitypoll_widget_access('results', $entitypoll)) {
      $result_machine_name = !empty($entitypoll_type->data['results'][$entitypoll_view_mode]['results']) ? $entitypoll_type->data['results'][$entitypoll_view_mode]['results'] : FALSE;

      $result_info = entitypoll_results_display_info();
      $result_display = !empty($result_info[$result_machine_name]) ? $result_info[$result_machine_name] : FALSE;
      if (!$result_display) {
        $result_output['#markup'] = t('No result.');
      } else {

        $results_config = !empty($entitypoll_type->data['results'][$view_mode]['config']) ? $entitypoll_type->data['results'][$view_mode]['config'] : array();

        // Load a file with callbacks.
        if (isset($result_display['file'])) {
          entitypoll_include_file($result_display['module'], $result_display['file']);
        }
        $result_output = $result_display['element callback']($entitypoll, $entitypoll_choice, $view_mode, $results_config, $votes);
      }
    }
    $content['entitypoll_choice_result'] = $result_output;

    return parent::buildContent($entitypoll_choice, $view_mode, $langcode, $content);
  }

  /**
   * {@inheritdoc}
   */
  function view($entities, $view_mode = 'full', $langcode = NULL, $page = NULL) {
    $view = parent::view($entities, $view_mode, $langcode, $page);
    // TODO replace '#theme' of entity with custom theme function?
    // (to remove title from template by default)
    return $view;
  }

  /**
   * Renders a single entity property.
   */
  protected function renderEntityProperty($wrapper, $name, $property, $view_mode, $langcode, &$content) {
    parent::renderEntityProperty($wrapper, $name, $property, $view_mode, $langcode, $content);
    $content[$name]['#label_hidden'] = TRUE;
  }
}
