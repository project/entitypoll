<?php


/**
 * exchanger_content entity edit/create form.
 */
function entitypoll_type_form($form, &$form_state, EntitypollType $entity = NULL, $type = NULL) {

  $form = array(
    '#type' => 'container',
  );

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Human readable name of this entitypoll type'),
    '#default_value' => isset($entity->label) ? $entity->label : '',
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($entity->name) ? $entity->name : '',
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'entitypoll_type_load',
      'source' => array('label'),
    ),
    '#weight' => -49,
    '#description' => t('A unique machine-readable name for this entitypoll type. It must only contain lowercase letters, numbers, and underscores.'),
    '#disabled' => !empty($entity->name),
  );

  $form['data']['config'] = array(
    '#title' => t('Type settings.'),
    '#type' => 'fieldset',
  );

  $voting_types = entitypoll_voting_type_info();

  $voting_types_options = array();
  foreach ($voting_types as $machine_name => $info) {
    $voting_types_options[$machine_name] = $info['name'];
  }

  $voting_type = !empty($entity->voting_type) ? $entity->voting_type : NULL;
  $form['data']['config']['voting_type'] = array(
    '#type' => 'select',
    '#title' => t('Type of voting'),
    '#description' => t('This value once set - cannot be changed.'),
    '#options' => $voting_types_options,
    '#default_value' => $voting_type,
    '#empty_option' => t('-- Not chosen --'),
    '#disabled' => !empty($voting_type),
    '#required' => TRUE,
  );

/*


  foreach ($voting_types_options as $machine_name => $name) {
    $form['data']['config']['voting_type']['config'][$machine_name] = array(
      '#type' => 'container',
      '#states' => array(
        'visible' => array(
          ':input[name="data[config][voting_type][voting_type]"]' => array('value' => $machine_name),
        ),
      ),
    );

    // Widget
    $form['data']['config']['voting_type']['config'][$machine_name]['widget'] = array(
      '#type' => 'container',
    );

    $widgets_options = array();
    $widgets_info = entitypoll_widget_info();
    foreach ($widgets_info as $widget_machine_name => $widget_info) {
      if ($machine_name == $widget_info['voting type']) {
        $widgets_options[$widget_machine_name] = $widget_info['name'];
      }
    }

    $default_widget = !empty($entity->data['config']['voting_type']['config'][$machine_name]['widget']['widget']) ? $entity->data['config']['voting_type']['config'][$machine_name]['widget']['widget'] : NULL;
    $form['data']['config']['voting_type']['config'][$machine_name]['widget']['widget'] = array(
      '#type' => 'select',
      '#title' => t('Widget'),
      '#options' => $widgets_options,
      '#empty_option' => t('-- Not chosen --'),
      '#default_value' => $default_widget,
    );

    // Results display
    $form['data']['config']['voting_type']['config'][$machine_name]['results_display'] = array(
      '#type' => 'container',
    );

    $results_displays_info = entitypoll_results_display_info();
    $results_displays_options = array();
    foreach ($results_displays_info as $results_display_machine_name => $results_display_info) {
      if ($machine_name == $results_display_info['voting type']) {
        $results_displays_options[$results_display_machine_name] = $results_display_info['name'];
      }
    }

    $default_results_display = !empty($entity->data['config']['voting_type']['config'][$machine_name]['results_display']['results_display']) ? $entity->data['config']['voting_type']['config'][$machine_name]['results_display']['results_display'] : NULL;
    $form['data']['config']['voting_type']['config'][$machine_name]['results_display']['results_display'] = array(
      '#type' => 'select',
      '#title' => t('Results display'),
      '#options' => $results_displays_options,
      '#empty_option' => t('-- Not chosen --'),
      '#default_value' => $default_results_display,
    );

  }
  $view_mode_settings = field_view_mode_settings('entitypoll', $entity->name);

  $enabled_view_modes = array();
  foreach ($view_mode_settings as $view_mode => $settings) {
    if ($settings['custom_settings']) {
      $enabled_view_modes[$view_mode] = $view_mode;
    }
  }
*/

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save entitypoll choice type'),
    ),
  );


  return $form;
}

/**
 * Submit handler for entitypoll form.
 */
function entitypoll_type_form_submit($form, &$form_state) {

  $entity = entity_ui_form_submit_build_entity($form, $form_state);
  $entity->save();

  entitypoll_type_cache_reset();
  entity_info_cache_clear();
  drupal_set_message(t('Entitypoll type has been saved.'));
}