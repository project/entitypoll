<?php


/**
 * exchanger_content entity edit/create form.
 */
function entitypoll_choice_type_form($form, &$form_state, EntitypollChoiceType $entity = NULL, $type = NULL) {

  // TODO Rest of the form

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Human readable name of this choice type'),
    '#default_value' => isset($entity->label) ? $entity->label : '',
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($entity->name) ? $entity->name : '',
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'entitypoll_choice_type_load',
      'source' => array('label'),
    ),
    '#weight' => -49,
    '#description' => t('A unique machine-readable name for this entitypoll choice type. It must only contain lowercase letters, numbers, and underscores.'),
    '#disabled' => !empty($entity->name),
  );

  // TODO lock possibility of changing "name" if it exists.

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save entitypoll choice type'),
    ),
  );

  return $form;
}

/**
 * Submit handler for entitypoll form.
 */
function entitypoll_choice_type_form_submit($form, &$form_state) {
  $entity = entity_ui_form_submit_build_entity($form, $form_state);
  $entity->save();
  entitypoll_type_cache_reset();
  entity_info_cache_clear();
  drupal_set_message(t('Entitypoll choice type has been saved.'));
}