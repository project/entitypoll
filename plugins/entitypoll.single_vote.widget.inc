<?php

/**
 * "Single vote" widget callback.
 */
function entitypoll_single_vote_widget_element_callback($entitypoll, $entitypoll_choice, $choice_view_mode, $config = array(), $votes = array()) {
  $form = array(
    '#type' => 'radio',
    '#name' => "entitypoll_choices[$entitypoll->entitypoll_id]",
    '#parents' => array('entitypoll_choices', $entitypoll->entitypoll_id),
    '#return_value' => $entitypoll_choice->pcid,
  );
  return $form;
}


/**
 * "One vote" saving vote callback.
 */
function entitypoll_single_vote_votes_callback($values) {
  $choice_id = reset($values);
  return array(
    $choice_id => 1,
  );
}