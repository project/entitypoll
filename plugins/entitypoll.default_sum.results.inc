<?php

/**
 * "Default sum" results display callback.
 */
function entitypoll_default_sum_results_element_display_callback($entitypoll, $entitypoll_choice, $choice_view_mode, $config = array(), $votes = array()) {

  $votes = $entitypoll_choice->getVotes('sum');
  $all_votes = $entitypoll->getVotes('sum');

  $max = max($all_votes);
  $min = min($all_votes);

  $class = array('entitypoll-choice-result-default-sum');

  if ($votes == 0) {
    $class[] = 'entitypoll-choice-result-empty';
  }
  if ($votes == $max) {
    $class[] = 'entitypoll-choice-result-max';
  }
  if ($votes == $min) {
    $class[] = 'entitypoll-choice-result-min';
  }

  $element = array();

  if ($max) {
    $percentage = round(($votes / $max) * 100);

    $element = array(
      '#type' => 'container',
      'votes' => array(
        '#markup' => "$percentage % ($votes)",
      ),
      'bar' => array(
        '#type' => 'container',

        '#attributes' => array(
          'class' => $class,
          'style' => "width: ${percentage}%;",
        ),
        '#attached' => array(
           'css' => array(drupal_get_path('module', 'entitypoll') . '/css/entitypoll.default_sum.results.css'),
        ),
      ),
    );
  }

  return $element;
}