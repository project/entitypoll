<?php

/**
 * Theme callback.
 */
function theme_entitypoll_choices($variables) {
  $element = $variables['element'];

  // Initialize the variable which will store our table rows.
  $rows = array();

  // Iterate over each element in our $form['choices']['choices'] array.
  foreach (element_children($element) as $id) {

    // Before we add our 'weight' column to the row, we need to give the
    // element a custom class so that it can be identified in the
    // drupal_add_tabledrag call.
    //
    // This could also have been done during the form declaration by adding
    // '#attributes' => array('class' => 'example-item-weight'),
    // directy to the 'weight' element in tabledrag_example_simple_form().
    $element[$id]['weight']['#attributes']['class'] = array('choice-item-weight');

    // We are now ready to add each element of our $form data to the $rows
    // array, so that they end up as individual table cells when rendered
    // in the final table.  We run each element through the drupal_render()
    // function to generate the final html markup for that element.

    $weight = drupal_render($element[$id]['weight']);
    $remove_button = drupal_render($element[$id]['remove']);

    unset($element[$id]['weight']);
    unset($element[$id]['remove_button']);

    $properties = drupal_render($element[$id]);

    $rows[] = array(
      'data' => array(
        // We are borrowing this class from field.css, we are not using fields,
        // but we want our choices to be styles similarly to fields.
        array('data' => '', 'class' => array('field-multiple-drag')),
        // Add our 'name' column.
        $properties,
        // Add remove button.
        $remove_button,
        // Add number of votes.

        // Add our 'weight' column.
        $weight,
      ),
      // To support the tabledrag behaviour, we need to assign each row of the
      // table a class attribute of 'draggable'. This will add the 'draggable'
      // class to the <tr> element for that row when the final table is
      // rendered.
      'class' => array('draggable'),
    );
  }

  // We now define the table header values.  Ensure that the 'header' count
  // matches the final column count for your table.
  $header = array(
    array(
      'data' => t('Choices'),
      'colspan' => 2,
    ),
    t('Remove'),
    t('Weight'),
  );

  // We also need to pass the drupal_add_tabledrag() function an id which will
  // be used to identify the <table> element containing our tabledrag form.
  // Because an element's 'id' should be unique on a page, make sure the value
  // you select is NOT the same as the form ID used in your form declaration.
  $table_id = 'choice-items-table';

  // We can render our tabledrag table for output.
  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
  ));

  //dpm($variables);

  // And then render any remaining form elements (such as our submit button).
  $output .= drupal_render_children($element);

  // We now call the drupal_add_tabledrag() function in order to add the
  // tabledrag.js goodness onto our page.
  //
  // For a basic sortable table, we need to pass it:
  // - the $table_id of our <table> element,
  // - the $action to be performed on our form items ('order'),
  // - a string describing where $action should be applied ('siblings'),
  // - and the class of the element containing our 'weight' element.
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'choice-item-weight');

  return $output;
}
