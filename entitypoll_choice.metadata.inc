<?php

/**
 * Extend the defaults.
 */
class EntitypollChoiceMetadataController extends EntityDefaultMetadataController {

  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $properties = &$info[$this->type]['properties'];

    // TODO: $properties['type']['options list'] = // List of types

    $properties['entitypoll'] = array(
      'label' => t("Entitypoll"),
      'type' => 'entitypoll',
      'description' => t("Entitypoll this choice belongs to."),
      'setter callback' => 'entity_property_verbatim_set',
      'schema field' => 'entitypoll',
    );

    $properties['created'] = array(
      'label' => t("Created"),
      'type' => 'date',
      'description' => t("The date entitypoll was created."),
      'setter callback' => 'entity_property_verbatim_set',
      'schema field' => 'created',
    );

    $properties['updated'] = array(
      'label' => t("Updated"),
      'type' => 'date',
      'description' => t("The date entitypoll was last time updated."),
      'setter callback' => 'entity_property_verbatim_set',
      'schema field' => 'updated',
    );

    $properties['weight'] = array(
      'label' => t("Weight"),
      'type' => 'integer',
      'description' => t("Weight of entitypoll choice."),
      'setter callback' => 'entity_property_verbatim_set',
      'schema field' => 'weight',
    );

    return $info;
  }
}