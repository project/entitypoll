<?php

/**
 * @file
 * Defines the inline entity form controller for Nodes.
 */

class EntitypollInlineEntityFormController extends EntityInlineEntityFormController {

  /**
  * Overrides EntityInlineEntityFormController::defaultLabels().
   */
  public function defaultLabels() {
    $labels = array(
      'singular' => t('poll'),
      'plural' => t('polls'),
    );
    return $labels;
  }

  /**
   * Overrides EntityInlineEntityFormController::tableFields().
   */
  public function tableFields($bundles) {
    $fields = parent::tableFields($bundles);
    //unset($fields['entitypoll_id']);

    //$name_key = 'bonds_bondholder_name';
    //$last_name_key = 'bonds_bondholder_last_name';
//
    //$fields[$name_key] = array(
    //  'type' => 'field',
    //  'label' => 'Imiona',
    //  'weight' => 2,
    //);
    //$fields[$last_name_key] = array(
    //  'type' => 'field',
    //  'label' => 'Nazwisko',
    //  'weight' => 3,
    //);

    return $fields;
  }

  /**
   * Overrides EntityInlineEntityFormController::entityForm().
   */
  public function entityForm($entity_form, &$form_state) {
    return entitypoll_form($entity_form, $form_state);
  }

  /**
   * Overrides EntityInlineEntityFormController::entityFormSubmit().
   */
  public function entityFormSubmit(&$entity_form, &$form_state) {
    parent::entityFormSubmit($entity_form, $form_state);
    entitypoll_form_submit_prepare($entity_form, $form_state);
  }

  /**
   * Overrides EntityInlineEntityFormController::save().
   */
  public function save($entity, $context) {
    entity_save($this->entityType, $entity);
  }
}
