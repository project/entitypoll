<?php

/**
 * Extra Fields Controller.
 */
class EntitypollExtraFieldsController extends EntityDefaultExtraFieldsController {

  public function fieldExtraFields() {
    $extra = parent::fieldExtraFields();
    $types = entitypoll_get_types();

    foreach ($types as $type) {
      $extra['entitypoll'][$type->name] = $extra['entitypoll']['entitypoll'];
      $extra['entitypoll'][$type->name]['form']['question'] = $extra['entitypoll'][$type->name]['display']['question'];

      $extra['entitypoll'][$type->name]['display']['entitypoll_widget'] = array(
        'label' => t('Entitypoll widget'),
        'description' => t('Entitypoll voting widget'),
        'weight' => 1,
      );
      $extra['entitypoll'][$type->name]['display']['entitypoll_results'] = array(
        'label' => t('Entitypoll results'),
        'description' => t('Entitypoll results'),
        'weight' => 1,
      );
    }
    return $extra;
  }
}
