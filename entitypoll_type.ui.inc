<?php

class EntitypollTypeUIController extends EntityDefaultUIController {
  /**
   * Provides definitions for implementing hook_menu().
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $stop = 0;

    //$items['admin/structure/entitypoll/entitypoll_types'] = array(
    //  'title' => 'Entitypoll Type',
    //  'page callback' => 'system_admin_menu_block_page',
    //  'access arguments' => array('entitypoll.administer'),
    //  'file' => 'system.admin.inc',
    //  'file path' => drupal_get_path('module', 'system'),
    //);

    return $items;
  }
}