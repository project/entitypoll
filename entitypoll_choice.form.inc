<?php


/**
 * Enttypoll choice edit/create form.
 */
function entitypoll_choice_form($form, &$form_state, EntitypollChoice $entity = NULL) {

  entitypoll_choice_form_attach_elements($entity, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save entitypoll choice'),
    ),
  );

  return $form;
}


/**
 * Attach proper elements to entitypoll choice form.
 */
function entitypoll_choice_form_attach_elements($entity, &$form, &$form_state) {
  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($entity->label) ? $entity->label : '',
    '#description' => t('Label of this choice.'),
    '#required' => TRUE,
    '#weight' => -50,
  );

  field_attach_form('entitypoll_choice', $entity, $form, $form_state);
}



/**
 * Implements hook_form_validate().
 *
 * @param $form
 * @param $form_state
 */
function entitypoll_choice_form_validate($form, &$form_state) {
  $form_state['values']['type'] = $form_state['entitypoll_choice']->type;
  entity_form_field_validate('entitypoll_choice', $form, $form_state);
}


/**
 * Submit handler for entitypoll form.
 */
function entitypoll_choice_form_submit($form, &$form_state) {

  $entity = entity_ui_form_submit_build_entity($form, $form_state);

  field_attach_submit('entitypoll_choice', $entity, $form, $form_state); // ??

  $entity->save();
  drupal_set_message(t('Entitypoll choice has been saved.'));
}
