<?php


/**
 * Class Entitypoll
 */
class Entitypoll extends Entity {
  /**
   * The internal ID of this content.
   *
   * @var int
   */
  public $entitypoll_id = 0;

  public $type;

  public $language;

  public $created;

  public $uid;

  public $updated;

  public $published;

  public $end_date;

  public $choices = array();

  /**
   * In this mode no default properties are set, you need to provide them
   * yourself.
   *
   * @var bool
   */
  public $migrate_mode = FALSE;


  public $poll_mode;

  /**
   * Creates a new entity.
   *
   * @see entity_create()
   */
  public function __construct(array $values = array()) {
    // If this is new then set the type first.
    if (isset($values['is_new'])) {
      $this->type = isset($values['type']) ? $values['type'] : '';
    }

    parent::__construct($values, 'entitypoll');
  }

  /**
   * Helper method for getting only choices ids.
   */
  public function getChoicesIds() {
    $choices = $this->choices;
    $ids = array();
    foreach ($choices as $choice) {
      $ids[] = $choice->pcid;
    }
    return $ids;
  }

  /**
   * Get array of votes for each choice.
   */
  public function getVotes($function) {
    $results = array();
    foreach ($this->choices as $choice) {
      $results[$choice->pcid] = $choice->getVotes($function);
    }
    return $results;
  }

}

/**
 * Class EntitypollController
 */
class EntitypollController extends EntityAPIController {

  /**
   * {@inheritdoc}
   */
  public function create(array $values = array()) {
    $values += array(
      'created' => REQUEST_TIME,
    );
    return parent::create($values);
  }

  /**
   * {@inheritdoc}
   */
  function load($ids = array(), $conditions = array()) {
    $entities = parent::load($ids, $conditions);
    if (!empty($entities)) {
      foreach ($entities as $entity) {
        // Cast non-string scalars to their original types, because some backends
        // store/return all variables as strings.
        $entity->entitypoll_id = (int) $entity->entitypoll_id;
      }
    }
    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  function attachLoad(&$queried_entities, $revision_id = FALSE) {
    parent::attachLoad($queried_entities, $revision_id);

    // If choices are not loaded for some entities - load them.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'entitypoll_choice', '=');
    $query->propertyCondition('entitypoll', array_keys($queried_entities), 'IN');
    $results = $query->execute();

    $choices = entitypoll_choice_load_multiple(array_keys($results['entitypoll_choice']));

    foreach ($queried_entities as &$entity) {
      foreach ($choices as $choice) {
        if ($choice->entitypoll == $entity->entitypoll_id) {
          $entity->choices[] = $choice;
        }
      }
    }
  }


  /**
   * {@inheritdoc}
   */
  function save($entity, DatabaseTransaction $transaction = NULL) {

    if ($entity->migrate_mode === FALSE) {
      if (isset($entity->is_new)) {
        $entity->created = REQUEST_TIME;
      }
      $entity->updated = REQUEST_TIME;
    }

    $save = parent::save($entity, $transaction);

    if ($save === FALSE) {
      return $save;
    }

    // Delete from database choices that are no more related
    $existing_choices = array();
    foreach ($entity->choices as $choice) {
      if (is_numeric($choice->pcid) && $choice->pcid > 0) {
        $existing_choices[] = $choice->pcid;
      }
    }
    $delete = db_delete('entitypoll_choice')
      ->condition('entitypoll', $entity->entitypoll_id, '=');
    if (!empty($existing_choices)) {
      $delete->condition('pcid', $existing_choices, 'NOT IN');
    }
    $delete->execute();

    // Save/update all choices
    if (!empty($entity->choices)) {
      foreach ($entity->choices as $choice) {
        $choice->entitypoll = $entity->entitypoll_id;
        if (entity_save('entitypoll_choice', $choice) === FALSE) {
          drupal_set_message(t('Entitypoll choice nr %weight couldn\'t be saved.', array('%weight' => $choice->weight)), 'error');
        }
      }
    }

    return $save;
  }



  /**
   * {@inheritdoc}
   */
  function delete($ids, DatabaseTransaction $transaction = NULL) {
    db_delete('entitypoll_choice')
      ->condition('entitypoll', $ids, 'IN')
      ->execute();
    parent::delete($ids, $transaction);
  }


  /**
   * {@inheritdoc}
   */
  public function buildContent($entitypoll, $view_mode = 'voting', $langcode = NULL, $content = array()) {

    $entitypoll_type = entitypoll_type_load($entitypoll->type);

    $results_view_mode = !empty($entitypoll_type->data['widget'][$view_mode]['results_display_view_mode']) ? $entitypoll_type->data['widget'][$view_mode]['results_display_view_mode'] : NULL;
    $widget_choices_view_mode = !empty($entitypoll_type->data['widget'][$view_mode]['choices_view_mode']) ? $entitypoll_type->data['widget'][$view_mode]['choices_view_mode'] : NULL;

    if ($widget_choices_view_mode === NULL || $results_view_mode === NULL) {
      $content['entitypoll_widget']['#markup'] = $content['entitypoll_results']['#markup'] = t('Choose existing view modes for poll widget and results output in entitypoll type configuration.');
      return parent::buildContent($entitypoll, $view_mode, $langcode, $content);
    }

    // Widget
    $widgets_info = entitypoll_widget_info();
    $widget = !empty($entitypoll_type->data['widget'][$view_mode]['widget']) ? $entitypoll_type->data['widget'][$view_mode]['widget'] : NULL;

    $widget_output = array();
    if (empty($widgets_info[$widget])) {
      $widget_output['#markup'] = t('Selected widget is not available, check your entitypoll type settings.');
    }
    else {
      if (entitypoll_widget_access('vote', $entitypoll)) {

        // Load a file with callbacks.
        if (isset($widgets_info[$widget]['file'])) {
          entitypoll_include_file($widgets_info[$widget]['module'], $widgets_info[$widget]['file']);
        }
        $widget_output = drupal_get_form('entitypoll_widget_display_form', $entitypoll, $entitypoll_type, $view_mode, $widget_choices_view_mode, $widget, $widgets_info[$widget]);
      }
      else {
        $widget_output['#markup'] = t('You are not allowed to vote.');
      }
    }
    $content['entitypoll_widget'] = $widget_output;


    // If user voted, then we are changing view mode to results.
    if (entitypoll_current_user_voted($entitypoll->entitypoll_id)) {
      $view_mode = $results_view_mode;
    }


    $results_info = entitypoll_results_display_info();
    $results = !empty($entitypoll_type->data['results'][$view_mode]['results']) ? $entitypoll_type->data['results'][$view_mode]['results'] : NULL;
    $results_choices_view_mode = !empty($entitypoll_type->data['results'][$view_mode]['choices_view_mode']) ? $entitypoll_type->data['results'][$view_mode]['choices_view_mode'] : NULL;

    $results_output = array();
    if (empty($results_info[$results])) {
      $results_output['#markup'] = t('Selected results display is not available, check your entitypoll type settings.');
    }
    else {
      if (entitypoll_widget_access('results', $entitypoll)) {
        $results_config = !empty($entitypoll_type->data['results'][$view_mode]['config']) ? $entitypoll_type->data['results'][$view_mode]['config'] : array();

        // Load a file with callbacks.
        if (isset($results_info[$results]['file'])) {
          entitypoll_include_file($results_info[$results]['module'], $results_info[$results]['file']);
        }
        $results_output = $results_info[$results]['results display callback']($entitypoll, $view_mode, $results_choices_view_mode, $results_config);
      }
      else {
        $results_output['#markup'] = t('You are not allowed to see results of this poll.');
      }
    }
    $content['entitypoll_results'] = $results_output;

    return parent::buildContent($entitypoll, $view_mode, $langcode, $content);
  }

}


/**
 * Default callback building a form of choices.
 */
function entitypoll_display_results_default_callback($entitypoll, $view_mode, $choices_view_mode, $results_config) { //($entitypoll, $choices_view_mode, $results_display_machine_name, $results_display) {
  $element = array();
  foreach ($entitypoll->choices as $choice) {
    $choice->entitypoll_view_mode = $view_mode;
    $element[] = entity_view('entitypoll_choice', array($choice), $choices_view_mode);
  }
  return $element;
}