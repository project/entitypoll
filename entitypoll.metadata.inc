<?php

/**
 * Extend the defaults.
 */
class EntitypollMetadataController extends EntityDefaultMetadataController {

  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $properties = &$info[$this->type]['properties'];

    $properties['question']['label'] = t('Question');
    $properties['question']['description'] = t('Question of the poll');
    $properties['question']['setter callback'] = 'entity_property_verbatim_set';

    //$properties['language']['setter callback'] = 'entity_property_verbatim_set';
    //$properties['language']['options list'] = 'entity_metadata_language_list';

    // TODO: $properties['type']['options list'] = // List of types

    $properties['uid'] = array(
      'label' => t("Author"),
      'type' => 'user',
      'description' => t("Author of entitypoll."),
      'setter callback' => 'entity_property_verbatim_set',
      'schema field' => 'uid',
    );

    $properties['created'] = array(
      'label' => t("Created"),
      'type' => 'date',
      'description' => t("The date entitypoll was created."),
      'setter callback' => 'entity_property_verbatim_set',
      'schema field' => 'created',
    );

    $properties['updated'] = array(
      'label' => t("Updated"),
      'type' => 'date',
      'description' => t("The date entitypoll was last time updated."),
      'setter callback' => 'entity_property_verbatim_set',
      'schema field' => 'updated',
    );

    $properties['published'] = array(
      'label' => t("Published"),
      'type' => 'boolean',
      'description' => t("Is entitypoll published?"),
      'setter callback' => 'entity_property_verbatim_set',
      'schema field' => 'published',
    );

    $properties['end_date'] = array(
      'label' => t("End date of the poll."),
      'type' => 'date',
      'description' => t("Date after which poll will be/has been locked."),
      'setter callback' => 'entity_property_verbatim_set',
      'schema field' => 'end_date',
    );

    return $info;
  }
}