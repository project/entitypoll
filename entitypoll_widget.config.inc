<?php

/**
 * Choices formatter configuration page callback.
 */
function entitypoll_widget_config_form($form, &$form_state, $entitypoll_type, $view_mode = 'default') {

  drupal_set_title(t('Widget display configuration of %entitypoll_type in %view_mode view mode: ', array('%entitypoll_type' => $entitypoll_type->label(), '%view_mode' => $view_mode)), PASS_THROUGH);

  $form_state['entitypoll_type'] = $entitypoll_type;
  $form_state['view_mode'] = $view_mode;

  $form['widget'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
  );

  $voting_type_info = entitypoll_voting_type_info();

  if (empty($voting_type_info[$entitypoll_type->voting_type])) {
    $form['widget']['error']['#markup'] = t('Voting type chosen for this entitypoll type is no longer available!');
    return $form;
  }

  // Create list of view modes.
  $entity_info = entity_get_info('entitypoll');
  $enabled_view_modes = array();
  foreach ($entity_info['view modes'] as $view_mode_name => $settings) {
    if ($settings['custom settings']) {
      $enabled_view_modes[$view_mode_name] = $settings['label'];
    }
  }

  // Create list of widgets
  $widgets_options = array();
  $widgets_info = entitypoll_widget_info();
  foreach ($widgets_info as $widget_machine_name => $widget_info) {
    if ($entitypoll_type->voting_type == $widget_info['voting type']) {
      $widgets_options[$widget_machine_name] = $widget_info['name'];
    }
  }


  $default_widget = !empty($entitypoll_type->data['widget'][$view_mode]['widget']) ? $entitypoll_type->data['widget'][$view_mode]['widget'] : NULL;
  $form['widget'][$view_mode]['widget'] = array(
    '#type' => 'select',
    '#title' => t('Widget'),
    '#options' => $widgets_options,
    '#empty_option' => t('-- Not chosen --'),
    '#default_value' => $default_widget,
  );


  // Create list of view modes.
  $entity_info = entity_get_info('entitypoll_choice');
  $enabled_choice_view_modes = array();
  foreach ($entity_info['view modes'] as $view_mode_name => $settings) {
    if ($settings['custom settings']) {
      $enabled_choice_view_modes[$view_mode_name] = $settings['label'];
    }
  }

  // Choices view mode
  $default_choices_view_mode = !empty($entitypoll_type->data['widget'][$view_mode]['choices_view_mode']) ? $entitypoll_type->data['widget'][$view_mode]['choices_view_mode'] : NULL;
  $form['widget'][$view_mode]['choices_view_mode'] = array(
    '#type' => 'select',
    '#title' => t('Choices view mode'),
    '#description' => t('View mode in which choices will be displayed. It is about entitypoll_choice entities.'),
    '#options' => $enabled_choice_view_modes,
    '#required' => TRUE,
    '#empty_option' => t('-- Not chosen --'),
    '#default_value' => $default_choices_view_mode,
  );

  foreach ($widgets_options as $widget_machine_name => $widget_name) {
    $form['widget'][$view_mode]['config'][$widget_machine_name] = array(
      '#type' => 'container',
      '#states' => array(
        'visible' => array(
          ":input[name=\"widget[$view_mode][widget]\"]" => array('value' => $widget_machine_name),
        ),
      ),
    );

    // We are allowing for widgets custom configuration.
    if (!empty($widgets_info[$widget_machine_name]['display configuration'])) {
      $form['widget'][$view_mode]['config'][$widget_machine_name]['config'] = $widgets_info[$widget_machine_name]['display configuration']($view_mode);
    }
  }


  $default_results_display = !empty($entitypoll_type->data['widget'][$view_mode]['results_display_view_mode']) ? $entitypoll_type->data['widget'][$view_mode]['results_display_view_mode'] : NULL;
  $form['widget'][$view_mode]['results_display_view_mode'] = array(
    '#type' => 'select',
    '#title' => t('Results view mode'),
    '#description' => t('View mode displaing results that will show after voting in the poll.'),
    '#options' => $enabled_view_modes,
    '#required' => TRUE,
    '#empty_option' => t('-- Not chosen --'),
    '#default_value' => $default_results_display,
  );

  // Inform if used view mode have been disabled.
  if (!empty($default_results_display) && empty($enabled_view_modes[$default_results_display])) {
    drupal_set_message(t('Previously chosen view mode is no longer available, you need to choose and save new one.'), 'error');
    $form['widget'][$view_mode]['results_display']['#attributes'] = array(
      'class' => array('error'),
    );
  }

  $form['view_mode'] = array(
    '#type' => 'value',
    '#value' => $view_mode,
  );

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
    ),
  );

  return $form;
}

/**
 * Submit callback.
 */
function entitypoll_widget_config_form_submit($form, &$form_state) {
  /** @var EntitypollType $entitypoll_type */
  $entitypoll_type = $form_state['entitypoll_type'];
  $view_mode = $form_state['values']['view_mode'];

  $entitypoll_type->data['widget'][$view_mode] = $form_state['values']['widget'][$view_mode];
  $entitypoll_type->save();

  entitypoll_type_cache_reset();
  drupal_set_message(t('Display configuration has been saved.') . $entitypoll_type->label());
}