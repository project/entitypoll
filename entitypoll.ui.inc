<?php

class EntitypollUIController extends EntityBundleableUIController {
  /**
   * Provides definitions for implementing hook_menu().
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    foreach ($items as $path => $item) {
      // We are unsetting paths that are not useful for us.
      if(strpos($path, 'manage')) {
        unset($items[$path]);
      }
    }
    // We are unsetting a type and changing a weight to make entitypoll/add
    // link appear below "Add content" link in "admin_menu". Default type set
    // by parent class is MENU_LOCAL_ACTION which makes weight of menu link
    // modified by -= 1000 by admin_menu, because it assumes that all
    // MENU_LOCAL_ACTION items should be on top.
    $items[$this->path . '/add']['weight'] = 1;
    unset($items[$this->path . '/add']['type']);

    return $items;
  }


  public function operationForm($form, &$form_state, $entity, $op) {
    $form = parent::operationForm($form, $form_state, $entity, $op);
    $form['actions']['cancel']['#href'] = $form['actions']['cancel']['#options']['path'] = 'entitypoll/' . $entity->entitypoll_id . '/edit';
    return $form;
  }
}